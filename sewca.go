package main

import (
	"bitbucket.org/sewca/sewca/postmark"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/mnbbrown/engine"
	htmlt "html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	textt "text/template"
)

type Data struct {
	Email                 string
	ParentsName           string
	ChildsName            string
	FirstAppointmentDate  string
	FirstAppointmentTime  string
	SecondAppointmentDate string
	SecondAppointmentTime string
}

type Bounce struct {
	ID            int
	Type          string
	TypeCode      int
	Name          string
	Tag           string
	MessageID     string
	Description   string
	Details       string
	Email         string
	BouncedAt     string
	DumpAvailable bool
	Inactive      bool
	CanActivate   bool
	Subject       string
}

// {
//   "ID": 42,
//   "Type": "HardBounce",
//   "TypeCode": 1,
//   "Name": "Hard bounce",
//   "Tag": "Test",
//   "MessageID": "883953f4-6105-42a2-a16a-77a8eac79483",
//   "Description": "The server was unable to deliver your message (ex: unknown user, mailbox not found).",
//   "Details": "Test bounce details",
//   "Email": "john@example.com",
//   "BouncedAt": "2014-08-01T13:28:10.2735393-04:00",
//   "DumpAvailable": true,
//   "Inactive": true,
//   "CanActivate": true,
//   "Subject": "Test subject"
// }

const dateValue = "Mon Jan 02 2006"

type Error struct {
	RawMessage       string `json:"raw_message,omitempty"`
	DeveloperMessage string `json:"developer_message,omitempty"`
	HumanMessage     string `json:"human_message,omitempty"`
	Code             int    `json:"error_code,omitempty"`
	Field            string `json:"field,omitempty"`
}

func (e *Error) Error() string {
	return e.DeveloperMessage
}

func (e *Error) JSONString() string {
	b, _ := json.MarshalIndent(&e, "	", "")
	return string(b)
}

func SendEmail(rw http.ResponseWriter, req *http.Request) {

	var errors []*Error

	data := &Data{}
	data.Email = req.FormValue("email")
	fmt.Println(data.Email)
	if data.Email == "" || data.Email == "undefined" {
		e := &Error{
			DeveloperMessage: "Form value email was empty",
			HumanMessage:     "Please enter an email address",
			Field:            "email",
		}
		errors = append(errors, e)
	}
	data.ParentsName = req.FormValue("parents-name")
	if data.ParentsName == "" {
		e := &Error{
			DeveloperMessage: "Form value parents-name was empty",
			HumanMessage:     "Please enter the name of the parents or caregivers.",
			Field:            "parents-name",
		}
		errors = append(errors, e)
	}
	data.ChildsName = req.FormValue("childs-name")
	if data.ChildsName == "" {
		e := &Error{
			DeveloperMessage: "Form value childs-name was empty",
			HumanMessage:     "Please enter the name of the child",
			Field:            "childs-name",
		}
		errors = append(errors, e)
	}
	data.FirstAppointmentDate = req.FormValue("first-date")
	if data.FirstAppointmentDate == "" {
		e := &Error{
			DeveloperMessage: "Form value first-date was empty",
			HumanMessage:     "Please enter date of the first appointment.",
			Field:            "first-date",
		}
		errors = append(errors, e)
	}
	data.FirstAppointmentTime = req.FormValue("first-time")
	if data.FirstAppointmentTime == "" {
		e := &Error{
			DeveloperMessage: "Form value first-time was empty",
			HumanMessage:     "Please enter the time of the first appointment.",
			Field:            "first-time",
		}
		errors = append(errors, e)
	}
	data.SecondAppointmentDate = req.FormValue("second-date")
	if data.SecondAppointmentDate == "" {
		e := &Error{
			DeveloperMessage: "Form value second-date was empty",
			HumanMessage:     "Please enter the date of the second appointment.",
			Field:            "second-date",
		}
		errors = append(errors, e)
	}
	data.SecondAppointmentTime = req.FormValue("second-time")
	if data.FirstAppointmentTime == "" {
		e := &Error{
			DeveloperMessage: "Form value second-time was empty",
			HumanMessage:     "Please enter the time of the second appointment.",
			Field:            "second-time",
		}
		errors = append(errors, e)
	}

	if len(errors) > 0 {
		engine.JSON(rw, errors, http.StatusBadRequest)
		return
	}

	msg := postmark.NewMessage()
	msg.AddTo(data.Email)
	msg.Subject = "Your Upcoming Appointments"

	var body bytes.Buffer
	t, err := htmlt.ParseFiles("templates/email.html")
	if err != nil {
		herr := &Error{DeveloperMessage: "Failed to parse template tempaltes/email.html", RawMessage: err.Error(), HumanMessage: "Something went wrong at our end. We're looking into it!", Code: 5001}
		l := herr.JSONString()
		fmt.Println(l)
		http.Error(rw, l, http.StatusInternalServerError)
		return
	}
	t.Execute(&body, data)
	msg.HtmlBody = body.String()

	body.Reset()
	tpt, err := textt.ParseFiles("templates/email.txt")
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	tpt.Execute(&body, data)
	msg.TextBody = body.String()

	msg.AddAttachment("background.pdf", "static/background.pdf")
	msg.AddAttachment("fees.pdf", "static/fees.pdf")

	resp, err := mailer.SendMessage(msg)
	if err != nil {
		log.Println(err.Error())
		log.Println(resp.Message)
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(200)
	fmt.Fprint(rw, "Success")
}

func ProcessBounce(rw http.ResponseWriter, req *http.Request) {
	defer req.Body.Close()
	jsonBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Println(err)
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	var bounce *Bounce
	err = json.Unmarshal(jsonBody, &bounce)
	if err != nil {
		log.Println(err)
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	msg := postmark.NewMessage()
	msg.AddTo("me@matthewbrown.io")
	msg.Subject = "Bounce Alert: " + bounce.Subject
	msg.TextBody = string(jsonBody)

	resp, err := mailer.SendMessage(msg)
	if err != nil {
		log.Println(err.Error())
		log.Println(resp.Message)
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(200)
	fmt.Fprint(rw, "OK")
}

type Open struct {
	Recipient string
}

// {
//   "FirstOpen": true,
//   "Client": {
//     "Name": "Chrome 35.0.1916.153",
//     "Company": "Google",
//     "Family": "Chrome"
//   },
//   "OS": {
//     "Name": "OS X 10.7 Lion",
//     "Company": "Apple Computer, Inc.",
//     "Family": "OS X 10"
//   },
//   "Platform": "WebMail",
//   "UserAgent": "Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/35.0.1916.153 Safari\/537.36",
//   "ReadSeconds": 5,
//   "Geo": {
//     "CountryISOCode": "RS",
//     "Country": "Serbia",
//     "RegionISOCode": "VO",
//     "Region": "Autonomna Pokrajina Vojvodina",
//     "City": "Novi Sad",
//     "Zip": "21000",
//     "Coords": "45.2517,19.8369",
//     "IP": "188.2.95.4"
//   },
//   "MessageID": "883953f4-6105-42a2-a16a-77a8eac79483",
//   "ReceivedAt": "2014-06-01T12:00:00",
//   "Tag": "welcome-email",
//   "Recipient": "john@example.com"
// }

func ProcessOpen(rw http.ResponseWriter, req *http.Request) {
	defer req.Body.Close()
	jsonBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Println(err)
		return
	}
	var open *Open
	err = json.Unmarshal(jsonBody, &open)
	if err != nil {
		log.Println(err)
		return
	}

	msg := postmark.NewMessage()
	msg.AddTo("me@matthewbrown.io")
	msg.Subject = "Open Alert: " + open.Recipient
	msg.TextBody = string(jsonBody)

	resp, err := mailer.SendMessage(msg)
	if err != nil {
		log.Println(err.Error())
		log.Println(resp.Message)
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(200)
}

var mailer *postmark.Client

func main() {
	log.Println("Starting Appointment Booker...")

	log.Println("Starting Application!")

	conf_postmark_key := os.Getenv("POSTMARK_KEY")
	if conf_postmark_key == "" {
		log.Fatalln("Postmark key [POSTMARK_KEY] is required, and not set.")
	}

	conf_postmark_from := os.Getenv("POSTMARK_FROM")
	if conf_postmark_from == "" {
		log.Fatalln("Postmark from address [POSTMARK_FROM] is required, and not set.")
	}

	log.Println("Connecting to Postmark")
	mailer = postmark.NewClient(conf_postmark_key, conf_postmark_from)

	router := engine.NewRouter()
	router.Get("/", func(rw http.ResponseWriter, req *http.Request) {
		http.Redirect(rw, req, "/app/", http.StatusTemporaryRedirect)
	})

	apiGroup := router.SubRouter("/api")
	apiGroup.Post("/emails/send", SendEmail)
	apiGroup.Post("/emails/bounce", ProcessBounce)
	apiGroup.Post("/emails/open", ProcessOpen)

	router.Static("/app", "web")

	addr := ":" + os.Getenv("PORT")
	log.Println("Serving at " + addr)
	err := http.ListenAndServe(addr, router)
	if err != nil {
		panic(err)
	}
	log.Println("Stopping Application!")
}
