var gulp = require('gulp');
var concat = require('gulp-concat'),
sass = require('gulp-sass'),
autoprefixer = require('gulp-autoprefixer'),
minifyCSS = require('gulp-minify-css'),
minifyHTML = require('gulp-minify-html'),
uglify = require('gulp-uglify'),
sourcemaps = require('gulp-sourcemaps'),
rename    = require('gulp-rename');

gulp.task('sass', function () {
    gulp.src('scss/*.scss')
    .pipe(sass({expanded: true, onError: function(e) { console.log(e); } }))
    .pipe(autoprefixer("last 2 versions", "> 1%", "ie 8"))
    .pipe(sourcemaps.init())
    .pipe(concat('style.css'))
    .pipe(minifyCSS())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('css/'));
});

gulp.task('watch', function(){
    gulp.watch('scss/*.scss', ['sass']);
});

gulp.task('default', ['watch']);
