(function(){
	'use strict';

	var auth = {};

	auth.AuthenticationService = angular.factory('AuthenticationService', ['$http', 'localStorageService', '$rootScope', function($http, localStorageService, $rootScope){
		return {
			isAuthenticated: function() {
				return (localStorageService.get('auth_token') !== null);
			},
			me: function() {
				return $http({
					method: "GET",
					url: "/auth/me"
				});
			},
			login: function(email, password) {
				var login = $http({
					method: "POST",
					url: "/auth/login",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param({email: email, password: password}),
				});

				login.success(function(result){
					localStorageService.set('auth_token', result.token);
					$rootScope.$broadcast('loginSuccess');
				})
				return login;
			},
			register: function(name, email, password) {
				var register = $http({
					method: "POST",
					url: "/auth/register",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $param({email: email, name: name, password: password})
				});
				register.success(function(result){
					$rootScope.$broadcast('registerSuccess');
				});
				return register;
			},
			logout: function(){
				localStorageService.clearAll();
				$rootScope.$broadcast('logoutSuccess');
			}
		}
	}]);


auth.LoginController =  angular.controller('LoginController', ['$scope', function($scope){
	$scope.credentials = {};

	$scope.login = function() {
		if ($scope.credentials.email != null && $scope.credentials.password != null){
			AuthenticationService.login($scope.credentials.email, $scope.credentials.password).success(function(){
				$location.path("/dashboard");
			}).error(function(status, data){
				if (status === 500) {
					$scope.error = "Something's wrong with Quoter at the moment. Please try again later";
				}else{
					$scope.error = "Invalid username or password";
				}
			})
		}
	}
}]);

window.auth = auth;

})();