(function(){
	'use strict';

	var app = angular.module('sewcaApp',["ngRoute","LocalStorageModule"]);

	app.config(['$routeProvider','$locationProvider', function($routeProvider, $locationProvider){

		$routeProvider.when("/auth/login", {
			templateUrl: "views/send_email.html",
			controller: "LoginController"
		})
		$routeProvider.when("/emails/send", {
			templateUrl: "views/send_email.html",
			controller: "SendEmailController"
		})
		.otherwise({
			redirectTo:"/emails/send"
		})

	}]);

	app.factory('AuthenticationService',  ['$http', 'localStorageService', '$rootScope', function($http, localStorageService, $rootScope){
		return {
			isAuthenticated: function() {
				return (localStorageService.get('auth_token') !== null);
			},
			me: function() {
				return $http({
					method: "GET",
					url: "/auth/me"
				});
			},
			login: function(email, password) {
				var login = $http({
					method: "POST",
					url: "/auth/login",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param({email: email, password: password}),
				});

				login.success(function(result){
					localStorageService.set('auth_token', result.token);
					$rootScope.$broadcast('loginSuccess');
				})
				return login;
			},
			register: function(name, email, password) {
				var register = $http({
					method: "POST",
					url: "/auth/register",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $param({email: email, name: name, password: password})
				});
				register.success(function(result){
					$rootScope.$broadcast('registerSuccess');
				});
				return register;
			},
			logout: function(){
				localStorageService.clearAll();
				$rootScope.$broadcast('logoutSuccess');
			}
		}
	}]);


app.factory('AuthInteceptor', ['$q','$injector','$location', function($q, $injector, $location){
	return {
		request: function(){
			var localStorageService = $injector.get('localStorageService');
			var token;
			if(localStorageService.get('auth_token')) {
				token = localStorageService.get('auth_token');
			}
			if (token) {
				config.headers.Authorization = 'Bearer ' + token;
			}
			if(options.api.token) {
				config.headers["X-API-Key"] = options.api.token;
			}
			return config;
		},
		responseError: function(response) {
			var localStorageService = $injector.get('localStorageService');
			if (response.status === 401) {
				localStorageService.clearAll();
				$location.path('/auth/login');
			}
			return $q.reject(response);
		}

	}
}]);


app.controller('LoginController', ['$scope', 'AuthenticationService', function($scope, AuthenticationService){
	$scope.credentials = {};

	$scope.login = function() {
		if ($scope.credentials.email != null && $scope.credentials.password != null){
			AuthenticationService.login($scope.credentials.email, $scope.credentials.password).success(function(){
				$location.path("/dashboard");
			}).error(function(status, data){
				if (status === 401) {
					$scope.error = "The username or password is incorrect!";
				}else{
					$scope.error = "Something went wrong. Please check our status page, try again later or contact support.";
				}
			})
		}
	}
}]);

	app.controller('SendEmailController', ['$scope', '$route', '$http', function($scope, $route, $http) {
		$scope.details = {};
		$scope.submitted = false;

		$scope.reset = function(){
			$route.reload();
		}

		$scope.send = function() {

			$scope.submitted = true;

			var data = {
						"email": $scope.details.email, 
						"parents-name": $scope.details.parentsName,
						"childs-name": $scope.details.childsName,
						"first-time": $scope.details.firstTime,
						"first-date": $scope.details.firstDate,
						"second-time": $scope.details.secondTime,
						"second-date": $scope.details.secondDate,
					};

			var request = $http({
					method: "POST",
					url: "/api/emails/send",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(stripNull(data)),
				});

			request.success(function(result){
				$scope.success = true;
			});
			request.error(function(result){
				$scope.submitted = false;
				$scope.errors = result;
			});
		}
	}]);

	function stripNull(obj) {
	  for (var i in obj) {
	    if (obj[i] === null || obj[i] === undefined) delete obj[i];
	  }
	  return obj;
	}

})();